package me.jcorn;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 * jCorn Development GmbH
 *
 * @author Julian Maierl
 * @author Markus Petritz
 * @version 1.0.0
 * @see http://jcorn.me
 */
public class ServerInterface extends javax.swing.JFrame {

    private int port;
    private ServerSocket serversocket;
    private Socket client;
    private ArrayList<DataOutputStream> clientList;
    private volatile Thread clientListener, dataSender;

    public ServerInterface() {
        initComponents();

    }

    private void setup() {
        port = Integer.parseInt(JOptionPane.showInputDialog("Please enter the Server Port"));
        clientList = new ArrayList<>();
        if (runServer()) {
            System.out.println("Listen to Clients");
            listenToClients();
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        btGenerate = new javax.swing.JButton();
        btSendMessage = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        taOutput = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(409, 261));

        jLabel1.setFont(new java.awt.Font("Open Sans", 1, 24)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Server");
        getContentPane().add(jLabel1, java.awt.BorderLayout.PAGE_START);

        jPanel1.setLayout(new java.awt.BorderLayout());

        btGenerate.setText("Generate and Send Random Numbers");
        btGenerate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onSendRandom(evt);
            }
        });
        jPanel2.add(btGenerate);

        btSendMessage.setText("Send Message");
        btSendMessage.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onSendMessage(evt);
            }
        });
        jPanel2.add(btSendMessage);

        jPanel1.add(jPanel2, java.awt.BorderLayout.PAGE_START);

        taOutput.setEditable(false);
        taOutput.setColumns(20);
        taOutput.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
        taOutput.setRows(5);
        jScrollPane1.setViewportView(taOutput);

        jPanel1.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void onSendRandom(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onSendRandom
        dataSender = null;
        NumberGenerator generator = new NumberGenerator(100, 55, 10000);
        generator.generate();
        int[] array = generator.getNumberArray();

        dataSender = new Thread(new DataSender(array));
        dataSender.start();

        appendToOutput("Sent Random Numbers");
    }//GEN-LAST:event_onSendRandom

    private void onSendMessage(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onSendMessage
        dataSender = null;
        int sArray[];
        String message = JOptionPane.showInputDialog("Please enter a message to send");
        sArray = new int[message.length()];

        for (int i = 0; i < message.length(); i++) {
            sArray[i] = message.charAt(i);
        }

        dataSender = new Thread(new DataSender(sArray));
        dataSender.start();
        
        appendToOutput("Sent Message");
    }//GEN-LAST:event_onSendMessage

    public static void main(String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ServerInterface.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ServerInterface.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ServerInterface.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ServerInterface.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                ServerInterface serverInterface = new ServerInterface();
                serverInterface.setVisible(true);
                serverInterface.setup();
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btGenerate;
    private javax.swing.JButton btSendMessage;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea taOutput;
    // End of variables declaration//GEN-END:variables

    private boolean runServer() {
        try {
            serversocket = new ServerSocket(port);
            return true;
        } catch (IOException ex) {
            Logger.getLogger(ServerInterface.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("NOOB");
        }

        return false;
    }

    private void listenToClients() {
        clientListener = new Thread(new ClientListener());
        clientListener.start();
    }

    private void appendToOutput(String output) {
        taOutput.append(output+"\n");
    }

    private class DataSender implements Runnable {

        int[] number;

        public DataSender(int[] random) {
            this.number = random;
        }

        @Override
        public void run() {
            for (DataOutputStream dos : clientList) {
                if (dos != null) {
                    try {
                        int len = number.length;
                        dos.writeInt(len);
                        for (int i = 0; i < len; i++) {
                            dos.writeInt(number[i]);
                        }
                    } catch (IOException ex) {
                        Logger.getLogger(ServerInterface.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

            }
        }

    }

    private class ClientListener implements Runnable {

        @Override
        public void run() {
            while (true) {
                try {
                    Socket socket = serversocket.accept();
                    DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
                    clientList.add(dos);
                    appendToOutput("Client " + socket.getInetAddress() + " connected");
                } catch (IOException ex) {
                    Logger.getLogger(ServerInterface.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }

    }
}
