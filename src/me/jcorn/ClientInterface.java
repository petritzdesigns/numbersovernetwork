package me.jcorn;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 * jCorn Development GmbH
 *
 * @author Julian Maierl
 * @author Markus Petritz
 * @version 1.0.0
 * @see http://jcorn.me
 */
public class ClientInterface extends javax.swing.JFrame {

    private String host;
    private int port;
    private Socket socket;
    private DataInputStream dis;
    private Thread numberListener;

    public ClientInterface() {
        initComponents();
    }

    private void setup() {
        int started = 1;
        started = JOptionPane.showConfirmDialog(null, "Did you start the server?");

        while (started == 1) {
            started = JOptionPane.showConfirmDialog(null, "Did you start the server?");
            JOptionPane.showMessageDialog(null, "Please start the server");
        }

        host = JOptionPane.showInputDialog("Enter Server IP");
        port = Integer.parseInt(JOptionPane.showInputDialog("Enter Server Port"));

        if (connectToServer()) {
            startNumberListener();
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        taOutput = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Client");
        getContentPane().setLayout(new java.awt.BorderLayout(5, 5));

        jLabel1.setFont(new java.awt.Font("Open Sans", 1, 24)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Client");
        getContentPane().add(jLabel1, java.awt.BorderLayout.PAGE_START);

        taOutput.setEditable(false);
        taOutput.setColumns(20);
        taOutput.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
        taOutput.setRows(5);
        jScrollPane1.setViewportView(taOutput);

        getContentPane().add(jScrollPane1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public static void main(String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ClientInterface.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ClientInterface.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ClientInterface.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ClientInterface.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                ClientInterface ci = new ClientInterface();
                ci.setVisible(true);
                ci.setup();
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea taOutput;
    // End of variables declaration//GEN-END:variables

    private boolean connectToServer() {
        try {
            socket = new Socket(host, port);
            dis = new DataInputStream(socket.getInputStream());
            appendToOutput("Connected to " + host + "\n");
            return true;
        } catch (IOException ex) {
            Logger.getLogger(ClientInterface.class.getName()).log(Level.SEVERE, null, ex);
        }

        return false;

    }

    private void startNumberListener() {
        numberListener = new Thread(new NumberListener(dis));
        numberListener.start();
    }

    private void appendToOutput(String output) {
        taOutput.append(output);
    }

    private class NumberListener implements Runnable {

        private DataInputStream dis;
        private int[] array;

        public NumberListener(DataInputStream dis) {
            this.dis = dis;
        }

        @Override
        public void run() {
            while (true) {
                String numbers = "";
                String message = "";
                try {
                    int len = dis.readInt();
                    array = new int[len];
                    for (int i = 1; i <= len; i++) {
                        array[i - 1] = dis.readInt();
                        int num = array[i - 1];
                        
                        if (num >= 'A' && num <= 'Z' || num >= 'a' && num <= 'z') {
                            message += (char) num;
                        } else {
                            numbers += num + " ";
                        }
                        if (i % 10 == 0 && !numbers.equals("")) {
                            numbers += "\n";
                        }
                    }

                    if(!message.equals(""))
                    {
                        appendToOutput("Received text: \n");
                        appendToOutput(message);
                    }
                    else
                    {
                        appendToOutput("Received numbers: \n");
                        appendToOutput(numbers);
                    }
                } catch (IOException ex) {
                    Logger.getLogger(ClientInterface.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }

    }
}
