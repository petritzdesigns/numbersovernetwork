package me.jcorn;

import java.util.Random;

/**
 * jCorn Development GmbH
 *
 * @author Julian Maierl
 * @author Markus Petritz
 * @version 1.0.0
 * @see http://jcorn.me
 */
public class NumberGenerator {

    private final int amt, mul, og;
    private int[] numberArray;

    public NumberGenerator(int amt) {
        this.amt = amt;
        this.mul = 0;
        this.og = 3200;
        setup();
    }

    public NumberGenerator(int amt, int mul) {
        this.amt = amt;
        this.mul = mul;
        this.og = 3200;
        setup();
    }
    
    public NumberGenerator(int amt, int mul, int og) {
        this.amt = amt;
        this.mul = mul;
        this.og = og;
        setup();
    }

    private void setup() {
        this.numberArray = new int[amt];
    }

    public void generate() {
        Random rd = new Random();
        for (int i = 0; i < numberArray.length; i++) {
            numberArray[i] = rd.nextInt(og) + 1;
        }
        if (mul != 0) {
            for (int i = 0; i < numberArray.length; i++) {
                numberArray[i] *= mul;
            }
        }
    }

    public int[] getNumberArray() {
        return numberArray;
    }
    
    public String getNumberArrayAsString() {
        String buffer = "";
        for(int i = 0; i < numberArray.length; i++) {
            buffer += numberArray[i];
            buffer += "\n";
        }
        return buffer;
    }

}
